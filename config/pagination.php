<?php

return [
    'default_size' => env('PAGINATION_DEFAULT_SIZE', 25),
];
