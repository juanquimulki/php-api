<?php
declare(strict_types=1);

namespace Tests\Feature\API\V1\CatFacts\Rest;

use App\Models\CatFact;
use Tests\TestCase;

class ShowControllerTest extends TestCase
{
    protected string $fact = 'This is a cat fact';

    protected ?CatFact $catFact;

    protected function setUp(): void
    {
        parent::setUp();

        $this->catFact = new CatFact();
        $this->catFact->fact = $this->fact;

        $this->catFact->save();
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        if ($this->catFact) {
            $this->cleanUpModels([$this->catFact]);
        }
    }

    public function testSuccess(): void
    {
        $response = $this->getJson("/api/v1/cat-facts/{$this->catFact->id}");

        $response->assertStatus(200);
        $response->assertJsonFragment([
            'id' => $this->catFact->id,
            'fact' => $this->fact,
        ]);
    }

    public function testNotFound(): void
    {
        $response = $this->getJson("/api/v1/cat-facts/6942069");

        $response->assertStatus(404);
    }
}
